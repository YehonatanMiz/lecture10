
#include <list>
#include <iostream>
#include <string>
#include <map>
#include <stack>
#include <algorithm>
#include <vector>
#include <set>

class Player {
public:
	Player(std::string name):
		_score(0), _name(name)
	{
		
	}
	std::list<std::string> _visited;
	std::string _name;
private:
	int _score;
	int _id;
	std::pair<int, int> _vboxMoney;
	std::stack<int> _ammo;
	std::set<std::string> _awards;
};



typedef std::pair<std::string, Player*> myPlayer;

int main() {
	Player* newPlayer = new Player("Adi");
	std::string password = "donkeydonkey123";
	// map string to Player* (key to value)
	std::map<std::string, Player*> players;
	players.insert(myPlayer(password, newPlayer));
	// get iterator to the player that has the password donkeydonkey123
	std::map<std::string, Player*>::iterator player = players.find("donkeydonkey123");
	// derefenrce!!
	std::cout << player->second->_name << std::endl;
	// also works. [must_be_a_key!!!!](the first)
	Player* player2 = players["donkeydonkey123"];
	std::cout << player2->_name << std::endl;
	// pushing elements to the list
	newPlayer->_visited.push_back("2");
	newPlayer->_visited.push_back("3");
	newPlayer->_visited.push_back("1");
	newPlayer->_visited.push_back("5");
	newPlayer->_visited.push_back("10");
	newPlayer->_visited.push_back("6");
	// define list<string> iterator. It will point to the beginning of the list 
	std::list<std::string>::iterator list_start = newPlayer->_visited.begin();
	std::list<std::string>::iterator list_end = newPlayer->_visited.end();
	// sort all the elements that between list_start to list_end
	std::sort(list_start, list_end);
	// iterate all the data structure - in this case, a list
	std::list<std::string>::iterator it;
	for (it = newPlayer->_visited.begin(); it != newPlayer->_visited.end(); it++) {
		std::cout << *it << std::endl;
	}

	return 0;

}
